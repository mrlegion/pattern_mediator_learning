﻿using System;
using System.Collections.Generic;
using System.IO;
using DrawingManager.Core.Common;
using DrawingManager.Core.Components;
using DrawingManager.Core.Interfaces;

namespace DrawingManager.Core
{
    public class Model : IMediator
    {
        #region Private Fields

        // Base Link to All Components
        private readonly WatcherComponent _watcher;
        private readonly LoggerComponent _logger;
        private readonly ManagerComponent _manager;
        private readonly StorekeeperComponent _storekeeper;
        private readonly ExploderComponent _exploder;

        private string _directory;

        #endregion

        #region Properties

        /// <summary>
        /// Получение или установка директории для отслеживания
        /// </summary>
        public string Directories
        {
            get => _directory;
            set
            {
                if (!Directory.Exists(value))
                    throw new DirectoryNotFoundException($"Select directory [{value}] is not found");
                _directory = value;
                Log($"Set new watching directory: [{value}]");
            }
        }

        /// <summary>
        /// Получение или установка списка позиций в наряде
        /// </summary>
        public Queue<string> Sections { get; set; }

        /// <summary>
        /// Получение или установка типа перемещения файлов
        /// </summary>
        public MoveType MoveType { get; set; }

        /// <summary>
        /// Получение или установка действия при нахождении дубликатов при копировании файлов
        /// </summary>
        public FileExistBehavior FileExistBehavior { get; set; }

        /// <summary>
        /// Получение или установка типа разбивания файлов в каталогах по форматам
        /// </summary>
        public ExploderBehavior ExploderBehavior { get; set; }

        #endregion

        #region Construct

        public Model()
        {
            _watcher = new WatcherComponent() { Mediator = this };
            _logger = new LoggerComponent { Mediator = this };
            _manager = new ManagerComponent() { Mediator = this };
            _storekeeper = new StorekeeperComponent() { Mediator = this };
            _exploder = new ExploderComponent() { Mediator = this };
        }

        #endregion

        #region Impliment IMediator

        public void Log(string message) => _logger.Log(message ?? "Work!");

        public void Notify(IComponent component, Events e, string message = null)
        {
            switch (e)
            {
                // Вызов действий на событие создание файла в директории
                case Events.FileCreateInDirectory:
                    FileCreateInDirectoryHandler();
                    break;

                // Вызов действий на событие нахождения маркера 
                case Events.FoundMarker:
                    FoundMarkerHandler();
                    break;

                // Вызов действий на событие конца перемещения файлов
                case Events.MoveEnding:
                    break;

                // Вызов действий на событие деления файлов в каталоге по формату
                case Events.ExplodeAllFolder:
                    ExplodeAllFolder();
                    break;

                // Вызов событий на событие деления файлов в выбраном каталоге по форматам
                case Events.ExplodeOneFolder:
                    break;

                // Вызов действий на событие записи в журнале событий
                case Events.Logging:
                    break;
            }

            Log(message);
        }

        #endregion

        #region Private Methods

        private void FileCreateInDirectoryHandler()
        {
            _manager.Add(_watcher.File);
        }

        private void FoundMarkerHandler()
        {
            var folder = Sections.Dequeue();

            // Проверка на пустоту
            if (folder == null)
                throw new ArgumentNullException(nameof(folder));

            // Разделение по папкам
            _storekeeper.MoveFiles(folder: Path.Combine(Directories, folder), files: _manager.Drawings, behavior: FileExistBehavior, moveType: MoveType);
            _manager.Clear();
        }

        // TODO: перевести после тестов данный метод в приватный
        public void ExplodeAllFolder()
        {
            _exploder.ExplodeDirectoryName = "Formats Directory";
            _exploder.Explode(list: _storekeeper.FilesCatalog, behavior: ExploderBehavior, parrent: _directory);
            _storekeeper.Clear();
        }

        #endregion

        #region Watcher Controll

        public void GoWatching()
        {
            if (!_watcher.IsInit)
                _watcher.Init();

            if (Directories == null)
                throw new ArgumentNullException(nameof(Directories), "Set watching directory");

            _watcher.Directory = Directories;
            _watcher.Start();
        }

        public void StopWatching() => _watcher.Stop();

        public void StartWatching()
        {
            // Проверяем инициализирован ли Watcher
            if (!_watcher.IsInit)
            {
                _logger.Log("ERROR! Before on start watching need initzialize him!");
                throw new ArgumentException("Before on start watching need initzialize him!");
            }

            // Проверяем установлена ли директория слежения
            if (_watcher.Directory != null)
                _watcher.Start();

            _logger.Log("ERROR! Before on start watching need set directory!");
            throw new ArgumentException("Before on start watching need set directory!");
        }

        #endregion
    }
}
