﻿using System.Collections.Generic;
using System.IO;
using DrawingManager.Core.Common;
using DrawingManager.Core.Interfaces;

namespace DrawingManager.Core.Components
{
    public class ManagerComponent : IComponent
    {
        #region Private Fields

        /// <summary>
        /// Внутрений список изображений в секции
        /// </summary>
        private List<Drawing> _drawings;

        #endregion

        #region Properties

        /// <summary>
        /// Получение или установка ссылки на посредника
        /// </summary>
        public IMediator Mediator { get; set; }

        /// <summary>
        /// Получение списка файлов изображений
        /// </summary>
        public List<Drawing> Drawings => _drawings;

        #endregion

        #region Constructs

        public ManagerComponent()
        {
            _drawings = new List<Drawing>();
        }

        #endregion

        #region Private Method

        

        #endregion

        #region Public Method

        /// <summary>
        /// Добавление нового элемента в список файлов
        /// </summary>
        /// <param name="file">Путь к файлу</param>
        public void Add(string file)
        {
            // Проверка файла
            if (!File.Exists(file))
                throw new FileNotFoundException($"[{file}] not found!");

            Drawing drawing = new Drawing(file);

            // Если формат изображения равен а3 и если на нем изображены метки маркера
            // Сообщаем посреднику, что мы нашли маркер
            if (drawing.Format == "a3" && drawing.IsMarker)
            {
                var message = $"File: [{drawing.FullPath}] is Marker! Go to next section";
                Mediator.Notify(this, Events.FoundMarker, message);
                File.Delete(drawing.FullPath);
                return;
            }

            _drawings.Add(drawing);
        }

        /// <summary>
        /// Очистка внутреннего списка
        /// </summary>
        public void Clear()
        {
            if (_drawings.Count > 0)
                _drawings.Clear();
        }

        #endregion
    }
}
