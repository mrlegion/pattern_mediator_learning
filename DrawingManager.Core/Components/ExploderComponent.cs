﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DrawingManager.Core.Common;
using DrawingManager.Core.Interfaces;

namespace DrawingManager.Core.Components
{
    /// <summary>
    /// Определяем в каком месте и как будет происходить разбиение по форматам
    /// </summary>
    public enum ExploderBehavior
    {
        /// <summary>
        /// Разбитие в родительском каталоге
        /// </summary>
        InTopDirectory = 0,

        /// <summary>
        /// Разбитие происходит в каждой папке
        /// </summary>
        InCurrentDirectory = 1
    }

    // Дополнительные возможности
    // TODO: Сделать возможность добавлять к папкам разбиения по форматам имена родительских каталогов
    // TODO: Сделать возможность добавлять к папкам разбиения по форматам имя наряда переданного от пользователя

    public class ExploderComponent : IComponent
    {
        #region Private Fields

        private string _parrent;

        #endregion

        #region Properties

        public ExploderBehavior Type { get; set; }

        public Dictionary<string, List<Drawing>> Files { get; set; }

        public string ExplodeDirectoryName { get; set; }

        #endregion

        #region Construct

        public ExploderComponent()
        {
            ExplodeDirectoryName = "Exploder Directory";
        }

        #endregion
        
        #region Impliment IComponent

        public IMediator Mediator { get; set; }

        #endregion

        #region Public Methods

        public void Explode()
        {
            // Проверка списка на пустоту
            if (Files.Count == 0 || Files == null)
                throw new ArgumentNullException(nameof(Files), "Список файлов не может быть Null или пустым");

            // Выполнение операции по разбиению папки
            switch (Type)
            {
                // Перенос и выполнение деления по форматам в родительском каталоге
                case ExploderBehavior.InTopDirectory:
                    InTopDirectoryExplode();
                    break;
                // Производить деление по форматам в каждой из папок
                case ExploderBehavior.InCurrentDirectory:
                    InCurrentDirectoryExplode();
                    break;
            }
        }

        public void Explode(Dictionary<string, List<Drawing>> list)
        {
            Files = list;
            Explode();
        }

        public void Explode(Dictionary<string, List<Drawing>> list, ExploderBehavior behavior)
        {
            Type = behavior;
            Explode(list: list);
        }

        public void Explode(Dictionary<string, List<Drawing>> list, ExploderBehavior behavior, string parrent)
        {
            if (!Directory.Exists(parrent))
                throw new DirectoryNotFoundException($"Директория [ {parrent} ] не найдена");

            _parrent = parrent;
            Explode(list: list, behavior: behavior);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Разбитие файлов происходит в родительской папке
        /// </summary>
        private void InTopDirectoryExplode()
        {
            var temp = new List<Drawing>();

            foreach (var folder in Files)
            {
                temp.AddRange(folder.Value);
            }

            var formats = temp.GroupBy(drawing => drawing.Format);
            var path = Path.Combine(_parrent, ExplodeDirectoryName);

            foreach (var format in formats)
            {
                var dir = Path.Combine(path, format.Key);
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);

                foreach (var drawing in format)
                    FileMoveHelper.Move(folder: dir, drawing: drawing, behavior: FileExistBehavior.Rename, moveType: MoveType.Copy);
            }
        }

        /// <summary>
        /// Разбитие файлов происходит в каждой из папок
        /// </summary>
        private void InCurrentDirectoryExplode()
        {
            foreach (var file in Files)
            {
                var group = file.Value.GroupBy(drawing => drawing.Format);
                var path = Path.Combine(file.Key, ExplodeDirectoryName);

                foreach (var format in group)
                {
                    var dir = Path.Combine(path, format.Key);
                    if (!Directory.Exists(dir))
                        Directory.CreateDirectory(dir);

                    foreach (var drawing in format)
                        FileMoveHelper.Move(folder: dir, drawing: drawing, behavior: FileExistBehavior.Rename, moveType: MoveType.Copy);
                }
            }
        }

        #endregion
    }
}
