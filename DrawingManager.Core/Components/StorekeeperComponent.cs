﻿using System.Collections.Generic;
using System.IO;
using DrawingManager.Core.Common;
using DrawingManager.Core.Interfaces;

namespace DrawingManager.Core.Components
{
    

    public class StorekeeperComponent : IComponent
    {
        #region Properties

        public Dictionary<string, List<Drawing>> FilesCatalog { get; private set; }

        #endregion

        /// <summary>
        /// Инициализация нового экземпляра класса <see cref="StorekeeperComponent"/>
        /// </summary>
        public StorekeeperComponent()
        {
            FilesCatalog = new Dictionary<string, List<Drawing>>();
        }

        #region Public Method

        /// <summary>
        /// Перемещение файлов в дополнительные каталоги
        /// </summary>
        /// <param name="folder">Новый каталог, в который требуется перенести файлы</param>
        /// <param name="files">Список файлов, требующие переноса</param>
        /// <param name="moveType">Тип переноса файлов</param>
        /// <param name="behavior">Тип для определения, как переностить файлы</param>
        public void MoveFiles(string folder, List<Drawing> files, MoveType moveType = MoveType.Move, FileExistBehavior behavior = FileExistBehavior.Rename)
        {
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
                Mediator.Log(message: $"Create new folder [{Path.GetFileName(folder)}] on directiory [{Directory.GetParent(folder)}] ");
            }

            foreach (var file in files)
            {
                FileMoveHelper.Move(folder: folder, drawing: file, behavior: behavior, moveType: moveType);
                Mediator.Log(message: $"Move File [{file.FileName}] to [{folder}]");
            }

            // Если такая папка уже существует в Dictionary,
            // То просто добавляем к существующим новые файлы
            if (FilesCatalog.ContainsKey(folder))
                FilesCatalog[folder] = new List<Drawing>(files);
            // Если такой папки нет в Dictionary, создаем запись
            else
                FilesCatalog.Add(folder, new List<Drawing>(files));

            Mediator.Log(message: "Moving file is complite");
            Mediator.Notify(component: this, e: Events.MoveEnding);
        }

        /// <summary>
        /// Очистка внутренего хранилища с директориями и файлами
        /// </summary>
        public void Clear() => FilesCatalog.Clear();

        #endregion

        #region IComponent Impliment

        /// <summary>
        /// Получение или установка ссылки на посредника
        /// </summary>
        public IMediator Mediator { get; set; }

        #endregion

        
    }
}