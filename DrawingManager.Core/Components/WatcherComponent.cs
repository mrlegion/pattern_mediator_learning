﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using DrawingManager.Core.Common;
using DrawingManager.Core.Interfaces;

namespace DrawingManager.Core.Components
{
    public class WatcherComponent : IComponent
    {
        #region Private Field

        private FileSystemWatcher _watcher;
        private string _directory;
        private string[] _filters;

        #endregion

        #region Properties

        public string File { get; set; }

        public bool IsInit { get; private set; }

        /// <summary>
        /// Получение или установка директории для отслеживания
        /// </summary>
        public string Directory
        {
            get => _directory;
            set
            {
                _directory = (!string.IsNullOrEmpty(value))
                    ? value
                    : throw new ArgumentException("Неверный формат директории");
                if (_watcher != null)
                    _watcher.Path = _directory;
            }
        }

        /// <summary>
        /// Получение массива фильтров для отслеживания файлов
        /// </summary>
        public string[] Filter
        {
            get => _filters;
            set => _filters = (value.Length == 0)
                ? throw new ArgumentException("Массив фильтров файлов не может быть пустым")
                : value;
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Создание нового объекта класса <see cref="WatcherComponent"/>
        /// </summary>
        /// <param name="directory">Директория, которую следует отслеживать</param>
        /// <param name="filters">Фильтр отслеживаемых файлов</param>
        public WatcherComponent(string directory, string[] filters)
        {
            _directory = (System.IO.Directory.Exists(directory)) ? directory : throw new DirectoryNotFoundException($"[ {directory} ] is not found!");
            _filters = (filters.Length > 0) ? filters : throw new ArgumentException($"Filters array can not be less zero!");

            Init();
        }

        /// <summary>
        /// Создание нового объекта класса <see cref="WatcherComponent"/>
        /// </summary>
        /// <param name="directory">Директория, которую следует отслеживать</param>
        public WatcherComponent(string directory) : this(directory, new []{"tif", "tiff"}) { }

        /// <summary>
        /// Создание нового объекта класса <see cref="WatcherComponent"/> с конструктором по-умолчанию 
        /// (ВНИМАНИЕ! Объект создается пустым и требует в дальнейшем установить директорию 
        /// для отслеживания, а так же при необходимости фильтры остлеживаемых файлов)
        /// </summary>
        public WatcherComponent()
        {
            _directory = null;
            _filters = new[] {"tif", "tiff"};
        }

        #endregion

        #region IMediator Impliment

        /// <summary>
        /// Получение или установка ссылки на посредника
        /// </summary>
        public IMediator Mediator { get; set; }

        #endregion

        #region Events

        private void OnCreated(object sender, FileSystemEventArgs args)
        {
            if (!Regex.IsMatch(args.FullPath, GetRegexp(), RegexOptions.IgnoreCase))
                return;

            Thread.Sleep(50);
            
            // Записываем файл, который был создан
            File = args.FullPath;

            // Генерируем сообщение для компонента логирования
            string message = $"Create a new file: [{Path.GetFileName(File)}]\tIn directory: [{System.IO.Directory.GetParent(File)}]";

            // Сообщаем посреднику, что в директории был создан файл
            Mediator.Notify(this, Events.FileCreateInDirectory, message);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Создание регулярного выражения для отслеживания расширений файлов на основе заданного массива фильтров
        /// </summary>
        /// <returns>Регулярное выражение</returns>
        private string GetRegexp()
        {
            string result = "";

            for (int i = 0; i < Filter.Length; i++)
            {
                result += $"\\.{Filter[i]}";
                if (i < Filter.Length - 1)
                    result += "|";
            }

            return result;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Инициализация watcher
        /// </summary>
        public void Init()
        {
            if (_watcher == null)
            {
                _watcher = new FileSystemWatcher();
                IsInit = true;
            }

            // Инициализация дополнительных параметров
            _watcher.Path = Directory;
            _watcher.Filter = "*.*";
            _watcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName |
                                    NotifyFilters.CreationTime | NotifyFilters.Size;

            // Инициализация Event
            _watcher.Created += OnCreated;
        }

        /// <summary>
        /// Запуск отслеживания директории
        /// </summary>
        public void Start()
        {
            _watcher.EnableRaisingEvents = true;
            Mediator.Notify(this, Events.Logging, "Watcher start watching!");
        }

        /// <summary>
        /// Остоновка отслеживания директории
        /// </summary>
        public void Stop()
        {
            _watcher.EnableRaisingEvents = false;
            Mediator.Notify(this, Events.Logging, "Watcher stop watching!");
        }

        #endregion
    }
}
