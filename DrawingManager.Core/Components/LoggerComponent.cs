﻿using System;
using DrawingManager.Core.Interfaces;

namespace DrawingManager.Core.Components
{
    public class LoggerComponent : IComponent
    {
        public IMediator Mediator { get; set; }

        public void Log(string s) => Console.WriteLine($"Logging status: {DateTime.Now:G}\t{s}");
    }
}
