﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using PP = DrawingManager.Core.Common.AllPointParams;

namespace DrawingManager.Core.Common
{
    public class Helper
    {
        private static int mWidth = 10;
        private static int mHeight = 10;
        private static readonly bool[] MarkMask = {true, true, true, true, true};

        /// <summary>
        /// Разрешенный диапозон отклонения в размерах файла
        /// </summary>
        public const int Range = 15;

        /// <summary>
        /// Стандартный размер площади стандартного формата
        /// </summary>
        public const double StandartArea = 62370;

        /// <summary>
        /// Общий список всех форматов с наименованием и их размерами
        /// </summary>
        public static Dictionary<string, int[]> Formats = new Dictionary<string, int[]>()
        {
            // Name, array [ width, height, area, min area, max area ]
            // Base formats name
            {"a6", new[] {148, 105, 15540, 14300, 16830}},
            {"a5", new[] {210, 148, 31080, 29315, 32895}},
            {"a4", new[] {297, 210, 62370, 59860, 64930}},
            {"a3", new[] {420, 297, 124740, 121180, 128350}},
            {"a2", new[] {594, 420, 249480, 244435, 254575}},
            {"a1", new[] {841, 594, 499554, 492404, 506754}},
            {"a0", new[] {1188, 841, 999108, 988152, 1009278}},
            // GOST formats name
            // {X} x 630
            {"297x630", new[] {630, 297, 187110, 182500, 191770}},
            {"420x630", new[] {630, 420, 264600, 259375, 269875}},
            {"594x630", new[] {630, 594, 374220, 368125, 380365}},
            // {X} x 1050
            {"297x1050", new[] {1050, 297, 311850, 305140, 318610}},
            {"420x1050", new[] {1050, 420, 441000, 433675, 448375}},
            {"594x1050", new[] {1050, 594, 623700, 615505, 631945}},
            // {X} x 1260
            {"297x1260", new[] {1260, 297, 374220, 366460, 382030}},
            {"420x1260", new[] {1260, 420, 529200, 520825, 537625}},
            {"594x1260", new[] {1260, 594, 748440, 739195, 757735}},
            // Others
            {"297x841", new[] {1260, 594, 249777, 244112, 255492}},
            {"420x841", new[] {1260, 594, 353220, 346940, 359550}},
            {"420x891", new[] {1260, 594, 374220, 367690, 380800}},
        };

        public static bool CheckOnMark(string file)
        {
            Bitmap image = new Bitmap(file);
            // Проверка на положение вертикальное или горизонтальное
            if (image.Height > image.Width)
                image.RotateFlip(RotateFlipType.Rotate90FlipNone);

            // Проверка на правильность положения листа маркетов
            PP.Top.IsEnable = CheckPoint(PP.Top.X, PP.Top.Y, image);
            // Если лист находиться верх ногами, то его следует повернуть на 180 градусов
            if (!PP.Top.IsEnable)
            {
                image.RotateFlip(RotateFlipType.Rotate180FlipNone);
                PP.Top.IsEnable = true;
            }

            // Проверка на центральную точку
            PP.Center.IsEnable = CheckPoint(PP.Center.X, PP.Center.Y, image);

            // Проверка маркеров параметров
            PP.Param01.IsEnable = CheckPoint(PP.Param01.X, PP.Param01.Y, image);
            PP.Param02.IsEnable = CheckPoint(PP.Param02.X, PP.Param02.Y, image);
            PP.Param03.IsEnable = CheckPoint(PP.Param03.X, PP.Param03.Y, image);
            PP.Param04.IsEnable = CheckPoint(PP.Param04.X, PP.Param04.Y, image);

            // Сброс изображения
            image.Dispose();

            // Создание маски параметров
            var mask = new[]
            {
                PP.Center.IsEnable,
                PP.Param01.IsEnable,
                PP.Param02.IsEnable,
                PP.Param03.IsEnable,
                PP.Param04.IsEnable,
            };

            // TODO: Продумать схему получше для проверки маски
            // Возможно стоит использовать побитовые операции
            // Данный вариант очень грязный!

            var count = 0;

            foreach (var m in mask)
            {
                if (m)
                    count++;
            }

            return MarkMask.Length == count;
        }

        private static bool CheckPoint(double x, double y, Bitmap image)
        {
            List<Color> matrix = new List<Color>();

            x = x / 25.4 * image.HorizontalResolution;
            y = y / 25.4 * image.VerticalResolution;

            for (int row = 0; row < mHeight; row++)
            {
                for (int col = 0; col < mWidth; col++)
                {
                    var point = image.GetPixel((int) x - (mWidth / 2) + col, (int) y - (mHeight / 2) + row);
                    matrix.Add(point);
                }
            }

            var result = matrix.FindAll(color => color.Name.ToLower() == "ff000000");
            return ((double) result.Count() / matrix.Count) > 0.7;
        }
    }
}
