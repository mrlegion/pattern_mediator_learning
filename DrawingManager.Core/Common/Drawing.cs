﻿using System;
using System.Drawing;
using System.IO;

namespace DrawingManager.Core.Common
{
    public class Drawing
    {
        /// <summary>
        /// Возвращает полный путь до файла
        /// </summary>
        public string FullPath { get; set; }

        /// <summary>
        /// Возвращает наименование файла
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Возвращает значение ширины
        /// </summary>
        public double Width { get; private set; }

        /// <summary>
        /// Возвращает значение высоты
        /// </summary>
        public double Height { get; private set; }

        /// <summary>
        /// Возвращает значение площади файла
        /// </summary>
        public double Area { get; private set; }

        /// <summary>
        /// Возвращает название формата файла
        /// </summary>
        public string Format { get; private set; }

        /// <summary>
        /// Возвращает общее количество собержащих в файле а4
        /// </summary>
        public int IncludeStandartFormats { get; private set; }

        /// <summary>
        /// Возвращает занчение проверки файла на принадлежность его к маркеру разделителя частей
        /// </summary>
        public bool IsMarker { get; private set; }

        public Drawing(string file)
        {
            // Проверяем файл на существование
            if (!File.Exists(file))
                throw new FileNotFoundException("Файл не существует!");

            FullPath = file;
            FileName = Path.GetFileName(FullPath);

            // Заполнение объекта
            Init();

            // Проверка на маркер
            IsMarker = (Format == "a3" && Helper.CheckOnMark(FullPath));

            // Просчет содержания стандартного формата
            IncludeStandartFormats = GetNumberIncludesStandartFormats();
        }

        /// <summary>
        /// Инициализация объекта изображения
        /// </summary>
        private void Init()
        {
            Bitmap image = new Bitmap(FullPath);

            // Инициализируем размеры изображения
            var width = image.Width / image.HorizontalResolution * 25.4;
            var height = image.Height / image.VerticalResolution * 25.4;
            var area = width * height;

            // Освобождаем память
            image.Dispose();

            // Проверка на ориентацию изображения
            bool isHorizontal = width > height;

            // Проверка к какому формату принадлежит данное изображение
            foreach (var format in Helper.Formats)
            {
                // Проверяем есть ли данный формат по площади в данных
                if (area > format.Value[3] && area < format.Value[4])
                {
                    // Проверяем ориентацию изображения и делаем ее вертикальной
                    if (!isHorizontal)
                    {
                        var temp = width;
                        width = height;
                        height = temp;
                    }

                    var widthInRange = (width > (format.Value[0] - Helper.Range)) &&
                                       (width < (format.Value[0] + Helper.Range));
                    var heightInRange = (height > (format.Value[1] - Helper.Range)) &&
                                        (height < (format.Value[1] + Helper.Range));

                    // Проверка на дублирующий размер площади у форматов
                    if (widthInRange && heightInRange)
                    {
                        Format = format.Key;
                        Width = format.Value[0];
                        Height = format.Value[1];
                        Area = format.Value[2];

                        return;
                    }
                }
            }

            Width = Math.Floor(width);
            Height = Math.Floor(height);
            Format = $"{Width}x{Height}";

            Area = Width * Height;
        }

        // TODO: Сделать проверку на меркеры

        // TODO: Проверить нужно ли это!
        private int GetNumberIncludesStandartFormats()
        {
            double result = Area / Helper.StandartArea;
            return (int)((result % 1 > 0.1) ? Math.Ceiling(result) : Math.Floor(result));
        }
    }
}