﻿using System.Collections.Generic;
using System.IO;

namespace DrawingManager.Core.Common
{
    public class FileReaderOnTxt
    {
        public Queue<string> ParseTxtFile(string file)
        {
            // Проверка файла на существование
            if (!File.Exists(file))
                throw new FileNotFoundException($"[{file}] is not found");

            // Открываем файл
            StreamReader reader = new StreamReader(File.OpenRead(file));
            Queue<string> queue = new Queue<string>();

            // Считываем информацию из файла в коллекцию
            var line = reader.ReadLine();
            while (line != null) 
            {
                queue.Enqueue(line);
                line = reader.ReadLine();
            }

            // Закрываем файл
            reader.Close();

            return queue;
        }
    }
}
