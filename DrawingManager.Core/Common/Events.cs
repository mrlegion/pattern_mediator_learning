﻿namespace DrawingManager.Core.Common
{
    /// <summary>
    /// Список типов событий обрабатываемые посредником
    /// </summary>
    public enum Events
    {
        /// <summary>
        /// В директории отслеживания был создан файл
        /// </summary>
        FileCreateInDirectory,

        /// <summary>
        /// Найден файл-маркер, который означает перевод секции
        /// </summary>
        FoundMarker,

        /// <summary>
        /// Перемещение файлов завершенно
        /// </summary>
        MoveEnding,

        /// <summary>
        /// Развить все файлы в каталогах по форматам
        /// </summary>
        ExplodeAllFolder,

        /// <summary>
        /// Разбить файлы в выбранном каталоге по форматам
        /// </summary>
        ExplodeOneFolder,

        /// <summary>
        /// Записать в журнал ведения событий
        /// </summary>
        Logging,
    }
}
