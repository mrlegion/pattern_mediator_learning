﻿namespace DrawingManager.Core.Common
{
    static class AllPointParams
    {
        public class Param01
        {
            public static double X { get; } = 55.0;
            public static double Y { get; } = 55.0;
            public static bool IsEnable { get; set; } = false;
        }

        public class Param02
        {
            public static double X { get; } = 55.0;
            public static double Y { get; } = 242.0;
            public static bool IsEnable { get; set; } = false;
        }

        public class Param03
        {
            public static double X { get; } = 365.0;
            public static double Y { get; } = 55.0;
            public static bool IsEnable { get; set; } = false;
        }

        public class Param04
        {
            public static double X { get; } = 365.0;
            public static double Y { get; } = 242.0;
            public static bool IsEnable { get; set; } = false;
        }

        public class Center
        {
            public static double X { get; } = 210.0;
            public static double Y { get; } = 148.5;
            public static bool IsEnable { get; set; } = false;
        }

        public class Top
        {
            public static double X { get; } = 210.0;
            public static double Y { get; } = 30.0;
            public static bool IsEnable { get; set; } = false;
        }
    }
}
