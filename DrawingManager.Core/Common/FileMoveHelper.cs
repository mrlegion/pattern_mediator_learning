﻿using System.IO;

namespace DrawingManager.Core.Common
{
    // TODO: Позже переместить
    /// <summary>
    /// Определяет тип операции при перемещении файла
    /// </summary>
    public enum FileExistBehavior
    {
        /// <summary>
        /// None: throw IOException "Конечный файл уже существует."
        /// </summary>
        None = 0,

        /// <summary>
        /// Replace: Заменить файл в конечной директории.
        /// </summary>
        Replace = 1,

        /// <summary>
        /// Skip: Пропустить файл.
        /// </summary>
        Skip = 2,

        /// <summary>
        /// Rename: Переименовать файл
        /// </summary>
        Rename = 3
    }

    // TODO: Позже переместить
    /// <summary>
    /// Определяет тип перемещения файла
    /// </summary>
    public enum MoveType
    {
        /// <summary>
        /// Copy: Копировать файлы в новую директорию
        /// </summary>
        Copy = 0,

        /// <summary>
        /// Move: перемещать все файлы (вырезать и переместить)
        /// </summary>
        Move = 1
    }

    public class FileMoveHelper
    {
        public static void Move(string folder,Drawing drawing, FileExistBehavior behavior = FileExistBehavior.Rename, MoveType moveType = MoveType.Move)
        {
            // Сохраняем старый путь до файла
            var from = drawing.FullPath;
            // Перезаписываем старый путь новым
            drawing.FullPath = Path.Combine(folder, drawing.FileName);

            switch (behavior)
            {
                case FileExistBehavior.Rename:
                    // Инициализируем счетчик копий
                    var count = 0;
                    // Получаем имя файла без расширения
                    var withoutExtantion = Path.GetFileNameWithoutExtension(drawing.FileName);
                    // Получаем расширение файла
                    var extantion = Path.GetExtension(drawing.FileName);

                    string pathWithDublicateIndex;

                    do
                    {
                        // Создаем новый путь для перемещения
                        drawing.FileName = $"{withoutExtantion}_[{++count}]{extantion}";
                        pathWithDublicateIndex = Path.Combine(folder, drawing.FileName);
                    }
                    while (File.Exists(pathWithDublicateIndex));
                    // Присваиваем новый путь в объекте изображения
                    drawing.FullPath = pathWithDublicateIndex;
                    break;
                case FileExistBehavior.Replace:
                    // Удаляем повторяещейся файл в финальной директории
                    File.Delete(drawing.FullPath);
                    break;
                case FileExistBehavior.Skip:
                    return;
                case FileExistBehavior.None:
                    throw new IOException("The destination file already exists.");
            }

            // Проверям какой тип перемещения был выбран
            switch (moveType)
            {
                case MoveType.Move:
                    // Перемещаем файл в номую директорию
                    File.Move(from, drawing.FullPath);
                    break;
                case MoveType.Copy:
                    // Копируем файл в новую директорию
                    File.Copy(from, drawing.FullPath);
                    break;
            }
        }
    }
}
