﻿namespace DrawingManager.Core.Interfaces
{
    public interface IComponent
    {
        IMediator Mediator { get; set; }
    }
}
