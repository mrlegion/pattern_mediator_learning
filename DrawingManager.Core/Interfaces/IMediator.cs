﻿using DrawingManager.Core.Common;

namespace DrawingManager.Core.Interfaces
{
    public interface IMediator
    {
        void Notify(IComponent component, Events e, string message = null);
        void Log(string message = null);
    }
}
