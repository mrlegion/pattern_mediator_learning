﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using DrawingManager.Core;
using DrawingManager.Core.Common;
using DrawingManager.Core.Components;

namespace Example.DM
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Drawing Manager v0.0.2a");
            Console.WriteLine(new string('-', 30));
            Console.WriteLine("Write Start to start wathing");
            Console.WriteLine("Press ESC to cancel watching\n");

            var command = Console.ReadLine()?.ToLower();

            if (command != "start" && command != "s" && command != "ы" && command != "с")
            {
                ExitMessage();
                return;
            }

            // Получаем список секций
            var reader = new FileReaderOnTxt();

            var model = new Model
            {
                Sections = reader.ParseTxtFile(@"S:\Новый текстовый документ.txt"),
                Directories = @"S:\",
                FileExistBehavior = FileExistBehavior.Replace,
                MoveType = MoveType.Move,
                ExploderBehavior = ExploderBehavior.InTopDirectory
            };

            // Start Watching
            model.GoWatching();

            while (true)
            {
                if (Console.ReadKey().Key == ConsoleKey.Escape)
                { 
                    // Stop Watching
                    model.StopWatching();
                    break;
                }
            }

            model.ExplodeAllFolder();

            //ExitMessage();

            // Delay
            //Console.ReadKey();
        }

        private static void ExitMessage()
        {
            Console.ForegroundColor = ConsoleColor.Red;

            Console.Write("Exit on programm.");

            for (int i = 0; i < 3; i++)
            {
                Thread.Sleep(500);
                Console.Write(".");
            }

            Thread.Sleep(500);
        }
    }
}
