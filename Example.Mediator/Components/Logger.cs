﻿using System;
using Example.Mediator.Interfaces;

namespace Example.Mediator.Components
{
    public class Logger : IComponent
    {
        public IMediator Mediator { get; set; }

        public void Log(double angle) => Console.WriteLine($"Logging pitch {angle} degress");
    }
}
