﻿using Example.Mediator.Common;
using Example.Mediator.Interfaces;

namespace Example.Mediator.Components
{
    public class AngleSensor : IComponent
    {
        private double _value;

        public double Value
        {
            get => _value;
            set
            {
                _value = value;
                Mediator.Notify(this, Events.AngleChanged);
            }
        }

        public IMediator Mediator { get; set; }
    }
}
