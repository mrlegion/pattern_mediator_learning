﻿using Example.Mediator.Common;
using Example.Mediator.Interfaces;

namespace Example.Mediator.Components
{
    public class MovementRegular : IComponent
    {
        public bool IsCancelled { get; set; } = false;

        public IMediator Mediator { get; set; }

        public void ExecuteStap()
        {
            if (IsCancelled)
                return;

            Mediator.Notify(this, Events.StepExecute);
        }

    }
}
