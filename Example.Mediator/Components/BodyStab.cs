﻿using System;
using Example.Mediator.Common;
using Example.Mediator.Interfaces;

namespace Example.Mediator.Components
{
    public class BodyStab : IComponent
    {
        public IMediator Mediator { get; set; }

        public void Stabilize()
        {
            Console.WriteLine("Stabilizing body");
            Mediator.Notify(this, Events.StabFinished);
        }
    }
}
