﻿using System;
using System.Threading;
using Example.Mediator.Common;
using Example.Mediator.Components;
using Example.Mediator.Interfaces;

namespace Example.Mediator
{
    public class CentralMediator : IMediator
    {
        private static Random _random = new Random();

        private AngleSensor _pitchAngleSensor;
        private AngleSensor _rotateAngleSensor;
        private AngleSensor _healingAngleSensor;
        private BodyStab _stab;
        private MovementRegular _movementRegular;
        private Logger _logger;

        public CentralMediator(AngleSensor pitchAngleSensor, AngleSensor rotateAngleSensor, AngleSensor healingAngleSensor, 
                               BodyStab stab, MovementRegular movementRegular, Logger logger)
        {
            _pitchAngleSensor = pitchAngleSensor;
            _rotateAngleSensor = rotateAngleSensor;
            _healingAngleSensor = healingAngleSensor;
            _stab = stab;
            _movementRegular = movementRegular;
            _logger = logger;
        }

        public void Notify(IComponent component, Events e)
        {
            switch (e)
            {
                case Events.StepExecute:
                    _pitchAngleSensor.Value = (Double) _random.Next(0, 10);
                    break;
                case Events.AngleChanged:
                    var pitchValue = _pitchAngleSensor.Value;
                    _logger.Log(pitchValue);
                    if (pitchValue > 0) _stab.Stabilize();
                    else Console.WriteLine("Stabilization is not needing");
                    break;
                case Events.StabFinished:
                    break;
            }
        }

        public void GoForward(int step)
        {
            for (int i = 0; i < step; i++)
            {
                if (_movementRegular.IsCancelled)
                    return;

                Thread.Sleep(1000);

                Console.WriteLine($"\n{i + 1} step forward");
                _movementRegular.ExecuteStap();
            }
        }
    }
}
