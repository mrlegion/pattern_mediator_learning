﻿namespace Example.Mediator.Interfaces
{
    public interface IComponent
    {
        IMediator Mediator { get; set; }
    }
}
