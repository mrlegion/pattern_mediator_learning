﻿using Example.Mediator.Common;

namespace Example.Mediator.Interfaces
{
    public interface IMediator
    {
        void Notify(IComponent component, Events e);
    }
}
