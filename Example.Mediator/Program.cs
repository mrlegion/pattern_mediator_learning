﻿using System;
using Example.Mediator.Components;

namespace Example.Mediator
{
    class Program
    {
        static void Main(string[] args)
        {
            AngleSensor pitchAngleSensor = new AngleSensor();
            AngleSensor rotateAngleSensor = new AngleSensor();
            AngleSensor healingAngleSensor = new AngleSensor();

            BodyStab stab = new BodyStab();
            MovementRegular movementRegular = new MovementRegular();
            Logger logger = new Logger();

            CentralMediator mediator = new CentralMediator(pitchAngleSensor, rotateAngleSensor, healingAngleSensor, stab, movementRegular, logger);

            pitchAngleSensor.Mediator = mediator;
            rotateAngleSensor.Mediator = mediator;
            healingAngleSensor.Mediator = mediator;

            stab.Mediator = mediator;
            movementRegular.Mediator = mediator;
            logger.Mediator = mediator;

            mediator.GoForward(10);

            // Delay
            Console.ReadKey();
        }
    }
}
