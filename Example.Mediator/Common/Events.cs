﻿namespace Example.Mediator.Common
{
    public enum Events
    {
        AngleChanged,
        StepExecute,
        StabFinished
    }
}
